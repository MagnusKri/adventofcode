﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Day11
{
    public class inputservice
    {

        public Gridd generateSquidGridFromInput(string path)
        {
            var nyliste = new List<Point>();
            var index = 0;
            var indexbortover = 0;
            foreach (string line in File.ReadLines(path))
            {
                foreach (char s in line)
                {
                    nyliste.Add(new Point(Int32.Parse(s.ToString()), indexbortover, index));
                    indexbortover++;
                }
                indexbortover = 0;
                index++;
            }

            return new Gridd(nyliste, index - 1);
        }
    }
}
