﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day11
{
    public class Point
    {
        public int x;
        public int y;
        public bool isLowPoint = false;
        public bool hasFlashed = false;
        public int value;
        public Point left;
        public Point up;
        public Point down;
        public Point right;
        public Point upright;
        public Point upleft;
        public Point downright;
        public Point downleft;



        public bool hasBeenDrawn = false;

        public Point(int value, int x, int y)
        {
            this.value = value;
            this.x = x;
            this.y = y;
        }

        public List<Point> getNaboer()
        {
            var list = new List<Point>();
            list.Add(left);
            list.Add(up);
            list.Add(down);
            list.Add(right);
            list.Add(upleft);
            list.Add(upright);
            list.Add(downleft);
            list.Add(downright);

            return list.Where(x => x != null).ToList();
        }
    }
}
