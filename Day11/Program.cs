﻿using System;
using System.Linq;

namespace Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"C:\Users\Magnus\source\repos\adventofcode\Day11\input.txt";
            var inpservice = new inputservice();
            var grid = inpservice.generateSquidGridFromInput(path);

            var counter = 0;
            var allflash = false;
            while(!allflash)
            {
                counter++;
                grid.step();
                allflash = grid.points.All(x => x.hasFlashed);
            }

            Console.WriteLine(grid.sumFlashes);
            Console.WriteLine(counter);
        }
    }
}
