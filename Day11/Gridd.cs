﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day11
{
    public class Gridd
    {
        int _size;
        public List<Point> points;
        public bool hasWon = false;
        public int sumFlashes = 0;

        public Gridd()
        {
        }

        public Gridd(List<Point> points, int size)
        {
            this.points = points;
            _size = size;
            initializenaboer();
        }

        public void initializenaboer()
        {
            foreach (Point p in points)
            {
                if (p.x != 0)
                    p.left = points.First(ps => ps.x == p.x - 1 && ps.y == p.y);

                if (p.y != 0)
                    p.up = points.First(ps => ps.x == p.x && ps.y + 1 == p.y);

                if (p.x != _size)
                    p.right = points.First(ps => ps.x == p.x + 1 && ps.y == p.y);

                if (p.y != _size)
                    p.down = points.First(ps => ps.x == p.x && ps.y - 1 == p.y);

               
            }

            foreach (Point p in points)
            {
                p.upleft = p.up?.left;
                p.downleft = p.down?.left;
                p.upright = p.up?.right;
                p.downright = p.down?.right;
            }
        }

        public int getSum()
        {
            return points.Where(p => p.isLowPoint).Select(p => p.value + 1).Sum();
        }

        public void drawOne(int number)
        {
            foreach(Point p in points)
            {
                if (p.value == number)
                    p.hasBeenDrawn = true;
            }

            checkVictory();
            
        }

        public void checkVictory()
        {
            for(int i = 0; i < _size+1; i++)
            {
                hasWon = points.Where(x => x.x == i).All(x => x.hasBeenDrawn);
                if (hasWon)
                    break;

                hasWon = points.Where(x => x.y == i).All(x => x.hasBeenDrawn);
                if (hasWon)
                    break;
            }
        }

        public void step()
        {
            foreach(Point p in points)
            {
                p.hasFlashed = false;
                p.value++;
            }
            foreach (Point p in points)
            {
                handleFlashes(p);
            }
            
        }

        public void handleFlashes(Point p)
        {
            if (p.hasFlashed)
                return;
            if (p.value > 9)
            {
                p.value = 0;
                p.hasFlashed = true;
                sumFlashes++;
                p.getNaboer().Where(n => !n.hasFlashed).ToList().ForEach(x => x.value++);
                
                foreach(Point lol in p.getNaboer().Where(n => !n.hasFlashed))
                {
                    handleFlashes(lol);
                }
            }

        }

        public void findBasin(Point p, ref List<Point> list)
        {
            if (!list.Contains(p))
                list.Add(p);
            else
                return;

            foreach (Point lol in p.getNaboer().Where(f => f.value != 9))
            {
                findBasin(lol, ref list);
            }

        }

    }
}
