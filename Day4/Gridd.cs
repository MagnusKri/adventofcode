﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day4
{
    public class Gridd
    {
        int _size;
        public List<Point> points;
        public bool hasWon = false;

        public Gridd()
        {
        }

        public Gridd(List<Point> points, int length, int bredde)
        {
            this.points = points;
            _size = length;
            initializenaboer();
        }

        public void initializenaboer()
        {
            foreach (Point p in points)
            {
                if (p.x != 0)
                    p.left = points.First(ps => ps.x == p.x - 1 && ps.y == p.y);

                if (p.y != 0)
                    p.up = points.First(ps => ps.x == p.x && ps.y + 1 == p.y);

                if (p.x != _size)
                    p.right = points.First(ps => ps.x == p.x + 1 && ps.y == p.y);

                if (p.y != _size)
                    p.down = points.First(ps => ps.x == p.x && ps.y - 1 == p.y);
            }

            foreach (Point p in points) {
                if(p.getNaboer().All(f => f.value > p.value))
                    p.isLowPoint = true;
                }
        }

        public int getSum()
        {
            return points.Where(p => p.isLowPoint).Select(p => p.value + 1).Sum();
        }

        public void drawOne(int number)
        {
            foreach(Point p in points)
            {
                if (p.value == number)
                    p.hasBeenDrawn = true;
            }

            checkVictory();
            
        }

        public void checkVictory()
        {
            for(int i = 0; i < _size+1; i++)
            {
                hasWon = points.Where(x => x.x == i).All(x => x.hasBeenDrawn);
                if (hasWon)
                    break;

                hasWon = points.Where(x => x.y == i).All(x => x.hasBeenDrawn);
                if (hasWon)
                    break;
            }
        }

    }
}
