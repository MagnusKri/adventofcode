﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day4
{
    public class Program
    {
        public static readonly string _pathboard = @"C:\Users\Magnus\source\repos\adventofcode\Day4\input.txt";
        public static readonly string _pathnumbers = @"C:\Users\Magnus\source\repos\adventofcode\Day4\inputNumbers.txt";

        static void Main(string[] args)
        {
            var inputservice = new inputservice();
            var nummer = inputservice.generateBingoNumbersFromInput(_pathnumbers);
            var brett = inputservice.generateBoardsFromInput(_pathboard);
            var vinnerbrett = new Gridd();
            int vinnernummber = 0;

            for(int i = 0; i < nummer.Count; i++)
            {
                foreach(Gridd g in brett)
                {
                    g.drawOne(nummer.ElementAt(i));
                    if (g.hasWon)
                    {
                        vinnerbrett = g;
                        vinnernummber = nummer.ElementAt(i);
                        break;
                    }
                }
                if (vinnerbrett.hasWon)
                    break;
            }

            var score = vinnerbrett.points.Where(x => !x.hasBeenDrawn).Select(x => x.value).Sum() * vinnernummber;
            Console.WriteLine(score);

            // PART 2
            var taperbrett = new Gridd();
            var tapernummber = 0;

            for (int i = 0; i < nummer.Count; i++)
            {
                foreach (Gridd g in brett)
                {
                    g.drawOne(nummer.ElementAt(i));
                    if (brett.Where(x => !x.hasWon).Count() == 0)
                    {
                        taperbrett = g;
                        tapernummber = nummer.ElementAt(i);
                        break;
                    }
                }
                if (brett.Where(x => !x.hasWon).Count() == 0)
                    break;
            }

            var taperscore = taperbrett.points.Where(x => !x.hasBeenDrawn).Select(x => x.value).Sum() * tapernummber;
            Console.WriteLine(taperscore);

            // startet 19:41 :) 
            // ferdig kl 21:04
        }


    }
}
