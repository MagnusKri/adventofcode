﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Day4
{
    public class inputservice
    {
        public List<int> generateBingoNumbersFromInput(string path)
        {
            var nyliste = new List<int>();

            var text = File.ReadAllText(path);

            foreach(string s in text.Split(','))
            {
                nyliste.Add(int.Parse(s));
            }

            return nyliste;
        }

        public List<Gridd> generateBoardsFromInput(string path)
        {
            var nyliste = new List<Gridd>();

            var brettpunkter = new List<Point>();
            var index = 0;
            var indexbortover = 0;

            foreach (string line in File.ReadLines(path))
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    nyliste.Add(new Gridd(brettpunkter, 4,4));
                    brettpunkter = new List<Point>();
                    index = 0;
                    continue;
                }

                foreach(string s in line.Split(' '))
                {
                    if (string.IsNullOrWhiteSpace(s))
                        continue;

                    brettpunkter.Add(new Point(int.Parse(s), indexbortover, index));
                    indexbortover++;
                }
                indexbortover = 0;
                index++;
            }

            return nyliste;
        }
    }
}
