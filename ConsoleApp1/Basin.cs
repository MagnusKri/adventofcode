﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Basin : IComparable
    {
        List<Point> points;
        public int size;

        public Basin(List<Point> pts)
        {
            points = pts;
            size = pts.Count;
        }

        public int CompareTo(object obj)
        {
            Basin obj2 = (Basin)obj;
            if (obj2.size == this.size)
                return 0;
            else if (obj2.size > size)
                return 1;
            else
                return -1;

        }
    }
}
