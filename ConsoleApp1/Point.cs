﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Point
    {
        public int x;
        public int y;
        public bool isLowPoint = false;
        public int value;
        public Point left;
        public Point up;
        public Point down;
        public Point right;

        public Point(int value, int x, int y)
        {
            this.value = value;
            this.x = x;
            this.y = y;
        }

        public List<Point> getNaboer()
        {
            var list = new List<Point>();
            if(left != null)
                list.Add(left);
            if (up != null)
                list.Add(up);
            if (down != null)
                list.Add(down);
            if (right != null)
                list.Add(right);

            return list;
        }
    }
}
