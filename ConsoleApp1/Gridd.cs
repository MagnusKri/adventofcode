﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    public class Gridd
    {
        int _size;
        List<Point> points;
        List<Basin> basins;

        public Gridd(List<Point> points, int length, int bredde)
        {
            this.points = points;
            this.basins = new List<Basin>();
            _size = length;
            initializenaboer();
            findBasins();
        }

        public void findBasins()
        {
            foreach (Point p in points.Where(f => f.isLowPoint))
            {
                var list = new List<Point>();
                findBasin(p, ref list);

                basins.Add(new Basin(list));
            }
        }

        public void findBasin(Point p, ref List<Point> list)
        {
            if (!list.Contains(p))
                list.Add(p);
            else
                return;

            foreach(Point lol in p.getNaboer().Where(f => f.value != 9))
                {
                    findBasin(lol, ref list);
                }
            
        }



        public void initializenaboer()
        {
            foreach (Point p in points)
            {
                if (p.x != 0)
                    p.left = points.First(ps => ps.x == p.x - 1 && ps.y == p.y);

                if (p.y != 0)
                    p.up = points.First(ps => ps.x == p.x && ps.y + 1 == p.y);

                if (p.x != _size)
                    p.right = points.First(ps => ps.x == p.x + 1 && ps.y == p.y);

                if (p.y != _size)
                    p.down = points.First(ps => ps.x == p.x && ps.y - 1 == p.y);
            }

            foreach (Point p in points) {
                if(p.getNaboer().All(f => f.value > p.value))
                    p.isLowPoint = true;
                }
        }
        public void lowpointsprint()
        {
            foreach(Point p in points.Where(fd => fd.isLowPoint))
            {
                Console.WriteLine(p.x + ", " + p.y);
            }
        }
        public int getSum()
        {
            return points.Where(p => p.isLowPoint).Select(p => p.value + 1).Sum();
        }

        public int threebiggestbasins()
        {
            basins.Sort();
            return basins.ElementAt(0).size * basins.ElementAt(1).size * basins.ElementAt(2).size;
        }
    }
}
