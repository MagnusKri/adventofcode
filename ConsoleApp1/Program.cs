﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
         private static readonly string _path = @"C:\Users\Magnus\source\repos\adventofcode\ConsoleApp1\heightmap.txt";

        static void Main(string[] args)
        {
            var dust = GenerateGridFromInput();
            // dust.lowpointsprint();
            Console.WriteLine(dust.getSum());
            Console.WriteLine(dust.threebiggestbasins());

        }

        public static Gridd GenerateGridFromInput()
        {
            var nyliste = new List<Point>();
            var index = 0;
            var indexbortover = 0;
            foreach (string line in  File.ReadLines(_path))
            {
               foreach(char s in line)
                {
                    nyliste.Add(new Point(Int32.Parse(s.ToString()), indexbortover, index));
                    indexbortover++;
                }
                indexbortover = 0;
                index++;
            }

            return new Gridd(nyliste, index - 1, index-1);
        }
    }
}
