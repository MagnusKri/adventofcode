﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Day10
{
    public class LineValidator
    {
        private bool isvalid = true;
        private readonly char[] åpnere = { '{', '<', '(', '[' };
        private readonly char[] lukkere = { '}', '>', ')', ']' };
        public LineValidator()
        {

        }

        public string removeAdjacentOpenCloses(string l)
        {
            var chs = l.ToCharArray();
            for(int i = 0; i < l.Length-1; i++)
            {
                if (chs[i] == '{')
                {
                    // out of index??
                    if(chs[i+1] == '}')
                    {
                        chs[i] = ',';
                        chs[i+1] = ',';
                    }
                }
                else if (chs[i] == '<')
                {
                    // out of index??
                    if (chs[i + 1] == '>')
                    {
                        chs[i] = ',';
                        chs[i+1] = ',';
                    }
                }
                else if (chs[i] == '[')
                {
                    // out of index??
                    if (chs[i + 1] == ']')
                    {
                        chs[i] = ',';
                        chs[i+1] = ',';
                    }
                }
                else if (chs[i] == '(')
                {
                    // out of index??
                    if (chs[i + 1] == ')')
                    {
                        chs[i] = ',';
                        chs[i+1] = ',';
                    }
                }
            }

            var nystring = string.Join(string.Empty, new string(chs).Split(','));
            // Console.WriteLine(nystring);
            return nystring;
        }

        public int calculateCorruptedLinePoints(string s)
        {
            var chs = s.ToCharArray();

            for (int i = 0; i < s.Length - 1; i++) {
                if (åpnere.Contains(chs[i + 1]))
                {
                    continue;
                }
                else if (chs[i] == '{')
                {
                    // out of index??
                    if (chs[i + 1] != '}')
                    {
                        return getSumOfSymbol(chs[i + 1]);
                    }
                }
                else if (chs[i] == '<')
                {
                    // out of index??
                    if (chs[i + 1] != '>')
                    {
                        return getSumOfSymbol(chs[i + 1]);
                    }
                }
                else if (chs[i] == '[')
                {
                    // out of index??
                    if (chs[i + 1] != ']')
                    {
                        return getSumOfSymbol(chs[i + 1]);
                    }
                }
                else if (chs[i] == '(')
                {
                    // out of index??
                    if (chs[i + 1] != ')')
                    {
                        return getSumOfSymbol(chs[i + 1]);
                    }
                }
            }

            return 0;
        }
        private int getSumOfSymbol(char c)
        {
            switch (c)
            {
                case ('}') :
                    return 1197;

                case ('>'):
                    return 25137;

                case (')'):
                    return 3;

                case (']'):
                    return 57;
            }

            Console.WriteLine("u fucked up");
            return 0; 
        }

        public string getEnding(string s)
        {
            var end = string.Empty;
            var s2 = s.Reverse();
            foreach(char c in s2)
            {
                end += getMatchingCharacter(c);
            }
            Console.WriteLine(end);
            return end;
        }

        private char getMatchingCharacter(char c)
        {
            if (åpnere.Contains(c))
                return lukkere[Array.IndexOf(åpnere, c)];
            else
                return åpnere[Array.IndexOf(lukkere, c)];
        }

        public BigInteger getScore(string s)
        {
            BigInteger sum = 0;
            foreach(char c in s)
            {
                sum *= 5;
                sum += getScoreOfSymbolPart2(c);
            }
            return sum;
        }

        private int getScoreOfSymbolPart2(char c)
        {
            switch (c)
            {
                case ('}'):
                    return 3;

                case ('>'):
                    return 4;

                case (')'):
                    return 1;

                case (']'):
                    return 2;
            }

            Console.WriteLine("u fucked up");
            return 0;
        }
    }
}
