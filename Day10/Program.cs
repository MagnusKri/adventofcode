﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Day10
{
    class Program
    {
        static readonly string path = @"C:\Users\Magnus\source\repos\adventofcode\Day10\input.txt";
        // static string validline = "[<>({}){}[([])<>]]";
        // static string brokenline = "{([(<{}[<>[]}>{[]{[(<()>";
        // static string test = "{{}{";

        static void Main(string[] args)
        {
            var inputservice = new inputservice();
            var inputs = inputservice.ReadLog(path);
            var validator = new LineValidator();
            var alteredinput = new List<string>();

            for (int i = 0; i < inputs.Count; i++)
            {
                Console.WriteLine(i);
                var validationComplete = false;
                var thisLine = inputs.ElementAt(i);
                var prevlength = thisLine.Length;
                while (!validationComplete)
                {
                    thisLine = validator.removeAdjacentOpenCloses(thisLine);
                    if(thisLine.Length == prevlength)
                    {
                        alteredinput.Add(thisLine);
                        validationComplete = true;
                    }
                    prevlength = thisLine.Length;
                }
            }

            var sum = 0;
            foreach(string s in alteredinput)
            {
                sum += validator.calculateCorruptedLinePoints(s);
            }
            Console.WriteLine(sum);

            // PART 2

            var endings = new List<string>();

            // reverserer bare input der alle gyldige er fjernet '()'
            foreach (string s in alteredinput.Where(x => validator.calculateCorruptedLinePoints(x) == 0))
            {
                endings.Add(validator.getEnding(s));
            }

            var scores = new List<BigInteger>();

            foreach(string s in endings)
            {
                scores.Add(validator.getScore(s));
            }

            scores.Sort();
            Console.WriteLine(scores.ElementAt(scores.Count / 2));

        }
    }
}
