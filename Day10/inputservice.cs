﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Day10
{
    public class inputservice
    {
        public List<string> ReadLog(string path)
        {
            var nyliste = new List<string>();

            foreach(string l in File.ReadAllLines(path))
            {
                nyliste.Add(l);
            }

            return nyliste;
        }
    }
}
